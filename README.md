# TFM-MasterCloudApps-Anuncios

Proyecto que importa las plantillas creadas en otro repositorio (https://gitlab.com/salva_ssc/tfm-mastercloudapps-templates) para facilitar la reutilización de pipelines para proyectos Java(maven). Estas plantillas se deberán de importar/incluir en el fichero .gitlab-ci.yml

Para la correcta utilización de las plantillas se deben definir las siguientes variables (Settings > CD/CI > Variables):

 - SONAR_LOGIN: Token acceso SonarCloud
 - CERTIFICATE_AWS: Certificado acceso AWS
 - URL_PRE: URL DNS pública máquina "PRE" 
 - URL_PRO: URL DNS pública máquina "PRO" 


Por otro lado en el fichero .gitlab-ci.yml también debe estar definidas estas variables:

  **tests**
   - CLASS_NAME_UNITARY_TEST: Nombre de la clase que contiene los test unitarios.
   - CLASS_NAME_SELENIUM_TEST: Nombre de la clase que contiene los test funcionales realizados con Selenium.
   - CLASS_NAME_REST_TEST: Nombre de la clase que contiene los test de APIs.

  **sonarqube**
   - SQ_PROJECTKEY: Nombre del proyecto 
   - SQ_ORGANIZATION: Organización SonarCloud donde volcar resultados

  **security**
    - VALUE_FAIL_BUILD_ON_CVSS: Valor máximo de criticidad permitido (1-10).

  **performance**
   - FILE_ARTILLERY: Nombre del fichero que contiene la configuración de artillery 

  **owasp_scan/deploy_aws**
   - APP_PORT: Puerto por defecto de la aplicación